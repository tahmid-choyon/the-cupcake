const fs = require('fs');

var addNote = (title, body) => {
    var allNotes = [];

    try {
        allNotes = JSON.parse(fs.readFileSync('notes-data.json'));
    } catch (ex) {

    }

    var duplicateNotes = allNotes.filter((note) => {
        return note.title === title;
    });

    if (duplicateNotes.length === 0) {
        newNote = {
            title,
            body
        };
        allNotes.push(newNote);
        fs.writeFileSync('notes-data.json', JSON.stringify(allNotes));
        console.log('New note added:', title);
    } else {
        console.log(`Note with title "${title}" already exists!`)
    }
}

var getAll = () => {
    var allNotes = [];
    try {
        allNotes = JSON.parse(fs.readFileSync('notes-data.json'));
    } catch (ex) {

    }

    if (allNotes.length === 0) {
        console.log('No notes are currently saved!')
    } else {
        allNotes.forEach((note) => console.log(`* ${note.title}`));
    }
}

var readNote = (title) => {
    var allNotes = [];

    try {
        allNotes = JSON.parse(fs.readFileSync('notes-data.json'));
    } catch (ex) {

    }

    var note = allNotes.filter((note) => {
        return note.title === title;
    });

    if (note.length === 0) {
        console.log('No note with this title exists!')
    } else {
        console.log(`Title: ${note[0].title}\nBody: ${note[0].body}`);
    }
}

var removeNote = (title) => {
    var allNotes = [];
    try {
        allNotes = JSON.parse(fs.readFileSync('notes-data.json'));
    } catch (ex) {

    }

    var newNoteList = [];
    allNotes.forEach((note) => {
        if (note.title !== title) {
            newNoteList.push(note);
        }
    });
    fs.writeFileSync('notes-data.json', JSON.stringify(newNoteList));
    console.log(`Note removed with title ${title}`);
}


module.exports = {
    addNote,
    getAll,
    readNote,
    removeNote
}