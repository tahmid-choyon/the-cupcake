package info.tahmidchoyon.javaspark.model;

import java.util.ArrayList;
import java.util.List;

public class SimpleCourseIdeaDAO implements CourseIdeaDAO {

    List<CourseIdea> ideas;

    String course_ideas[] = {"Spark Microframework", "Spring Basics", "CSS", "REST API with Spark", "Complete Laravel", "PHP", "Ruby on Rails"};
    String authors[] = {"Tahmid", "Rafiul", "Saadat", "Eshpelin", "Tanvir", "Craig", "John"};

    public SimpleCourseIdeaDAO() {
        ideas = new ArrayList<CourseIdea>();

        for (int i = 0; i < course_ideas.length; i++) {
            CourseIdea courseIdea = new CourseIdea(course_ideas[i], authors[i]);
            ideas.add(courseIdea);
        }
    }

    @Override
    public boolean add(CourseIdea idea) {
        return ideas.add(idea);
    }

    @Override
    public List<CourseIdea> findAll() {
        return new ArrayList<CourseIdea>(ideas);
    }

    @Override
    public CourseIdea findBySlug(String slug) {
        return ideas.stream()
                .filter(idea -> idea.getSlug().equals(slug))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

}
