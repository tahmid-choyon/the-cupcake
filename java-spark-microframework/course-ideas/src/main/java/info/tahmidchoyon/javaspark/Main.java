package info.tahmidchoyon.javaspark;

import info.tahmidchoyon.javaspark.model.CourseIdea;
import info.tahmidchoyon.javaspark.model.CourseIdeaDAO;
import info.tahmidchoyon.javaspark.model.SimpleCourseIdeaDAO;
import spark.ModelAndView;
import spark.template.handlebars.HandlebarsTemplateEngine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static spark.Spark.*;

public class Main {
    public static void main(String[] args) {

        staticFileLocation("public");

        CourseIdeaDAO dao = new SimpleCourseIdeaDAO();

        before("/ideas", (request, response) -> {
            if (request.cookie("username") == null) {
                response.redirect("/");
                halt();
            }
        });

        get("/", (request, response) -> {

            Map<String, String> cookieModel = null;

            if (request.cookie("username") != null) {
                cookieModel = new HashMap<>();
                cookieModel.put("username", request.cookie("username"));
            }
            return new ModelAndView(cookieModel, "index.hbs");
        }, new HandlebarsTemplateEngine());

        post("/", (request, response) -> {

            String username = request.queryParams("username");

            response.cookie("username", username);
            response.redirect("/");

            return null;
        });

        get("/ideas", (request, response) -> {

            Map<String, Object> model = new HashMap<>();
            model.put("ideas", dao.findAll());

            return new ModelAndView(model, "ideas.hbs");
        }, new HandlebarsTemplateEngine());

        post("/ideas", (request, response) -> {

            Map<String, Object> model = new HashMap<>();

            String idea = request.queryParams("idea");
            dao.add(new CourseIdea(idea, request.cookie("username")));

            response.redirect("/ideas");
            return null;
        }, new HandlebarsTemplateEngine());

        post("/ideas/:slug/votes", (request, response) -> {

            String slug = request.params("slug");
            CourseIdea courseIdea = dao.findBySlug(slug);
            courseIdea.addVote(request.cookie("username"));

            String requestPath = request.pathInfo();
            response.redirect(requestPath.substring(0, requestPath.length() - 6));
            return null;
        });

        get("/ideas/:slug", (request, response) -> {

            String slug = request.params("slug");
            CourseIdea courseIdea = dao.findBySlug(slug);

            List<String> voters = courseIdea.getVoters();

            Map<String, Object> model = new HashMap<>();

            model.put("courseIdea", courseIdea.getIdea());
            model.put("numberOfVoters", courseIdea.getVoteCount());
            model.put("voters", voters);
            model.put("slug", slug);

            return new ModelAndView(model, "course_idea.hbs");
        }, new HandlebarsTemplateEngine());
    }
}
