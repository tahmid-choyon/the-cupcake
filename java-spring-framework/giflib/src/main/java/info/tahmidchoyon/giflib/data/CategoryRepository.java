package info.tahmidchoyon.giflib.data;

import info.tahmidchoyon.giflib.model.Category;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class CategoryRepository {

    private static final List<Category> ALL_CATEGORIES = Arrays.asList(
            new Category(1, "cyborg"),
            new Category(2, "android"),
            new Category(3, "human")
    );

    public static List<Category> getAllCategories() {
        return new ArrayList<>(ALL_CATEGORIES);
    }

    public Category findById(int id) {
        for (Category category : ALL_CATEGORIES) {
            if (category.getId() == id) {
                return category;
            }
        }

        return null;
    }
}
