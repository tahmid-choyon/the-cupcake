package com.tahmidchoyon.springapp.services;

import com.tahmidchoyon.springapp.models.ShoppingList;
import com.tahmidchoyon.springapp.reporitories.ShoppingListRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ShoppingListService {
    
    private ShoppingListRepo repo;

    public ShoppingListService() {
        this.repo = new ShoppingListRepo();
    }

    public ShoppingList addShoppingList(ShoppingList shoppingList) {
        this.repo.shoppingLists.add(shoppingList);
        return shoppingList;
    }

    public ShoppingList findShoppingList(int shoppingListId) {
        return this.repo.shoppingLists
                .stream()
                .filter(s -> s.getId() == shoppingListId)
                .findFirst().get();
    }

    public void deleteShoppingList(int shoppingListId) {
        ShoppingList shoppingList = findShoppingList(shoppingListId);
        this.repo.shoppingLists.remove(shoppingList);
    }

    public List<ShoppingList> getAllShoppingList() {
        List<ShoppingList> cloneShoppingList = new ArrayList<>();
        this.repo.shoppingLists
                .stream()
                .forEach(cloneShoppingList::add);
        return cloneShoppingList;
    }

    public ShoppingList updateShoppingList(int shoppingListId, String title) {
        ShoppingList shoppingList = findShoppingList(shoppingListId);
        shoppingList.setTitle(title);
        return shoppingList;
    }
}
