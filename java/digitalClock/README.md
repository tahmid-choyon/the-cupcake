# digitalClock
This tiny project is an example of multithreading in Java.
<br>
A very simple digital clock desktop application which shows you the current time and animates some icons.

<img src="https://media.giphy.com/media/3og0IERrg4mNXWOICI/giphy.gif" width="422" height="240">
