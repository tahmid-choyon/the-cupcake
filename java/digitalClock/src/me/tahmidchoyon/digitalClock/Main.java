/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.tahmidchoyon.digitalClock;

/**
 *
 * @author tahmid
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {

        clockWindow clWindow = new clockWindow(); //creating object of clockWindow
        clWindow.setVisible(true); //setting visibility to TRUE
        Thread mainWindow = new Thread(clWindow); //initializing thread for clockWindow class
        mainWindow.setPriority(10); //setting top priority to mainWindow

        threadController thController = new threadController(); //threadController object creation
        Thread controllerThread = new Thread(thController); //initializg thread for threadController class
        controllerThread.setPriority(9); //setting priority level 9 to threadController's thread

        //starting both threads
        mainWindow.start();
        controllerThread.start();

    }
}
