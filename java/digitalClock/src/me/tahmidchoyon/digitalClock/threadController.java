/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.tahmidchoyon.digitalClock;

/**
 *
 * @author tahmid
 */
public class threadController implements Runnable {

    @Override
    public void run() {

        try {
            /*
            * calling the static method setTime() from clockWindow class
            * this setTime() method updates current time to same timeLabel variable over and over again
             */
            clockWindow.setTime();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

}
