from flask import jsonify, request
from flask_restless import APIManager
from model import app, db, Questions, Options

api_manager = APIManager(app=app, flask_sqlalchemy_db=db)

api_manager.create_api(Questions, methods=['GET', 'POST', 'PUT', 'DELETE'],
                       exclude_columns=['added', 'options.id', 'options.question_id'], url_prefix='/api/v1')


def add_cors_header(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'HEAD, GET, POST, PATCH, PUT, OPTIONS, DELETE'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type, *'
    response.headers['Access-Control-Allow-Credentials'] = 'true'

    return response


app.after_request(add_cors_header)


@app.route('/')
def home():
    return jsonify(
        {
            "message": "PO Basic API"
        }
    ), 200


@app.route('/add_questions', methods=['POST'])
def add_question():
    data = request.get_json()

    question_body = data['question_body']
    question = Questions(body=question_body)
    db.session.add(question)
    db.session.commit()

    for option in data['options']:
        op = Options(question.id, option['text'], option['is_correct'])
        db.session.add(op)
        db.session.commit()

    return jsonify(
        {
            "message": "added"
        }
    ), 200
