class foo():
    name = "foo"

    def foobar(self):
        print("foo.foobar()")


class A(foo):
    name = "A"

    def count(self):
        print("A.count()")

    def unique_A(self):
        print("A.unique_A()")

    def foobar(self):
        print("A.foobar()")


class B():
    name = "B"

    def count(self):
        print("B.count()")

    def unique_B(self):
        print("B.unique_B()")


class C(foo):
    name = "C"

    def count(self):
        print("C.count()")

    def unique_C(self):
        print("C.unique_C()")

    def foobar(self):
        print("C.foobar()")


class D(A, B, C):
    name = "D"

    def unique_D(self):
        print("D.unique_D()")


class E(C, A, B):

    def unique_E(self):
        print("E.unique_E()")


if __name__ == "__main__":
    d = D()
    print(d.name)  # prints "D" for obvious reason
    d.count()  # prints "A.count()", because class is "A" inherited first
    d.foobar()  # prints "A.foobar()", because immidiate parents are checked first then goes up. Bootom up approach

    e = E()
    print(e.name)  # prints "C" because class "E" has no attribute "name" and it goes up to its first parent class "C" and prints it from there
    e.foobar()  # prints "C.foobar()", because inheritance order matters.
