from flask import Flask, render_template, request, jsonify
from google_api import shorten_url

app = Flask(__name__)


@app.route('/', methods=['GET'])
def home():
    if request.method == 'GET':
        return render_template("index.html")


@app.route('/short', methods=['POST'])
def shorten():
    long_url = request.form['url']
    shortened_url = shorten_url(long_url=long_url)
    return str(shortened_url)


if __name__ == '__main__':
    app.run(debug=True)
