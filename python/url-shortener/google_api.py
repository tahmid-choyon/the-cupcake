import json
import requests

API_KEY = '<YOUR_API_KEY>'
URL = 'https://www.googleapis.com/urlshortener/v1/url?key={}'.format(API_KEY)
HEADER = {'Content-type': 'application/json'}


def shorten_url(long_url):
    data = json.dumps(
        {
            "longUrl": long_url
        }
    )

    api_response = requests.post(url=URL, data=data, headers=HEADER)
    response = json.loads(api_response.text)
    if 'error' in response:
        return response['error']['message']
    else:
        return response['id']
