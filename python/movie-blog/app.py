from flask import Flask, render_template, url_for, request, session, flash, redirect, abort
from flask_sqlalchemy import SQLAlchemy
from databaseModel import movies, imdb_movies
from util import imdbid_from_name
from CRUD import add_imdb_movie

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:letsplay@localhost:3306/movie_database'
db = SQLAlchemy(app)


@app.route('/')
def home():
    return render_template('base.html')


@app.route('/login')
def login():
    return render_template('login.html')


@app.route('/show_movies')
def show_movies():
    m = movies.query.all()
    return render_template('show_movies.html', movies=m)


@app.route('/all_movies')
def all_movies():
    mov = imdb_movies.query.limit(3)
    return render_template('all_movies.html', movies=mov)


@app.route('/search/<text>', methods=['GET', 'POST'])
def search_movie(text):
    if request.method == 'POST' or request.method == 'GET':
        text = request.form['search_box']
        if text == '':
            redirect(url_for('home'))
        text = str(text).replace(' ', '+')
        id = imdbid_from_name(text)
        if id is None:
            return render_template('bad_request.html', search_string=text)
        movie = imdb_movies(imdb_id=id)
        if movie is not None:
            add_imdb_movie(imdb_id=movie.imdb_id)
            return render_template('search_result.html', m=movie)
        else:
            return render_template('bad_request.html', search_string=text)


if __name__ == '__main__':
    app.run(debug=True)
