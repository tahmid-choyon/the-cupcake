from django.shortcuts import render, redirect
from django.http import HttpResponseBadRequest
from .models import Comment

from .forms import CommentForm


def index(request):
    comments = Comment.objects.order_by('-date_added')
    context = {
        'comments': comments
    }
    return render(request, 'index.html', context)


def sign(request):
    if request.method == 'POST':
        form = CommentForm(request.POST)

        if not form.is_valid():
            return HttpResponseBadRequest(content=b'BAD REQUEST')

        comment = Comment(name=request.POST['name'], comment=request.POST['comment'])
        comment.save()

        return redirect('index')
    elif request.method == 'GET':
        form = CommentForm()
        context = {
            'form': form
        }
        return render(request, 'sign.html', context)
