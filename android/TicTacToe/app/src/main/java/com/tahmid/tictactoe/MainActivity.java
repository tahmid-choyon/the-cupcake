package com.tahmid.tictactoe;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView, playerIconImageView;
    private int activePlayer = 1; // 1 = yellow, 0 = red
    private int gameState[] = {2, 2, 2, 2, 2, 2, 2, 2, 2};
    private final int winingPositions[][] = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6}};
    private LinearLayout linearLayout;
    private GridLayout gridLayout;
    private TextView textView, activePlayerTextView;
    private boolean gameIsActive;
    private Button resetButton;
    private MediaPlayer tickPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        linearLayout.bringToFront();
        linearLayout.setVisibility(View.INVISIBLE);

        gridLayout = (GridLayout) findViewById(R.id.gridLayout);

        tickPlayer = MediaPlayer.create(getApplicationContext(), R.raw.tick);

        textView = (TextView) findViewById(R.id.gameStatusTextView);
        textView.setText("<EMPTY>");

        playerIconImageView = (ImageView) findViewById(R.id.playerIcon);
        playerIconImageView.setImageResource(R.drawable.yellow);

        activePlayerTextView = (TextView) findViewById(R.id.activePlayerTextView);
        activePlayerTextView.setText("Yellow's Turn");

        resetButton = (Button) findViewById(R.id.resetButton);
        resetButton.setEnabled(true);

        gameIsActive = true;
    }

    public void dropIn(View view) {

        if (gameState[Integer.parseInt(view.getTag().toString())] == 2 && gameIsActive) {

            if (activePlayer == 1) {

                activePlayer = 0;
                gameState[Integer.parseInt(view.getTag().toString())] = 0;
                imageView = (ImageView) view;
                imageView.setTranslationY(-1000f);
                imageView.setImageResource(R.drawable.yellow);
                imageView.animate().translationYBy(1000f).rotation(720).setDuration(250);
                playerIconImageView.setImageResource(R.drawable.red);
                tickPlayer.start();
                activePlayerTextView.setText("Red's Turn");
            } else {

                activePlayer = 1;
                gameState[Integer.parseInt(view.getTag().toString())] = 1;
                imageView = (ImageView) view;
                imageView.setTranslationY(-1000f);
                imageView.setImageResource(R.drawable.red);
                imageView.animate().translationYBy(1000f).rotation(720).setDuration(250);
                playerIconImageView.setImageResource(R.drawable.yellow);
                tickPlayer.start();
                activePlayerTextView.setText("Yellow's Turn");
            }
            boolean isDraw = true;
            for (int cell : gameState) {
                if (cell == 2) {
                    isDraw = false;
                    break;
                }
            }

            if (isDraw) {
                gameIsActive = false;
                textView.setText("It's a draw!");
                linearLayout.setVisibility(View.VISIBLE);
                resetButton.setEnabled(false);
                playerIconImageView.setVisibility(View.INVISIBLE);
                activePlayerTextView.setVisibility(View.INVISIBLE);
            }

            for (int winingPosition[] : winingPositions) {

                if (gameState[winingPosition[0]] == gameState[winingPosition[1]]
                        && gameState[winingPosition[1]] == gameState[winingPosition[2]]
                        && gameState[winingPosition[0]] != 2) {

                    String winner = gameState[winingPosition[0]] == 0 ? "Yellow" : "Red";

//                    Toast.makeText(this, winner + " has won!", Toast.LENGTH_SHORT).show();

                    gameIsActive = false;
                    textView.setText(winner + " has won the game!");
                    linearLayout.setVisibility(View.VISIBLE);
                    resetButton.setEnabled(false);
                    playerIconImageView.setVisibility(View.INVISIBLE);
                    activePlayerTextView.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    public void playAgain(View view) {
        resetGame();
    }

    public void resetGame() {
        Log.d("MainActivity", "Resetting game...");
        gameIsActive = true;
        activePlayer = 1;
        Arrays.fill(gameState, 2);
        linearLayout.setVisibility(View.INVISIBLE);
        resetCell();
        playerIconImageView.setImageResource(R.drawable.yellow);
        activePlayerTextView.setText("Yellow's Turn");
        resetButton.setEnabled(true);
        playerIconImageView.setVisibility(View.VISIBLE);
        activePlayerTextView.setVisibility(View.VISIBLE);
    }

    private void resetCell() {

        for (int i = 0; i < gridLayout.getChildCount(); i++) {
            ((ImageView) gridLayout.getChildAt(i)).setImageResource(0);
        }
    }

    public void resetGame(View view) {
        resetGame();
    }
}