package info.tahmidchoyon.vehiclecounter.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sdsmdg.tastytoast.TastyToast;

import java.util.ArrayList;

import info.tahmidchoyon.vehiclecounter.model.Record;

/**
 * Created by tahmid on 05-Nov-17.
 */

public class DatabaseOperations {

    private static final String databaseName = "VehicleCounter";
    private static final String tableName = "count";
    private static final int version = 1;
    private static DatabaseOperations databaseOperations;
    private static SQLiteHelper sqLiteHelper;
    private Context context;
    private SQLiteDatabase mySqLiteDatabase;

    private DatabaseOperations(Context context) {
        this.context = context;
        sqLiteHelper = new SQLiteHelper(context, databaseName, version);
        createDatabase();
        createTable();
    }

    public static DatabaseOperations getInstance(Context context) {
        if (databaseOperations == null)
            databaseOperations = new DatabaseOperations(context);
        return databaseOperations;
    }

    private void createDatabase() {
        mySqLiteDatabase = sqLiteHelper.getWritableDatabase();
    }

    private void createTable() {
        String query = "CREATE TABLE IF NOT EXISTS `count` (\n" +
                "\t`id`\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`date_recorded`\tTEXT,\n" +
                "\t`start_time`\tTEXT,\n" +
                "\t`end_time`\tTEXT,\n" +
                "\t`bus`\tINTEGER,\n" +
                "\t`truck`\tINTEGER,\n" +
                "\t`car`\tINTEGER,\n" +
                "\t`bike`\tINTEGER,\n" +
                "\t`cng`\tINTEGER,\n" +
                "\t`cycle`\tINTEGER,\n" +
                "\t`rickshaw`\tINTEGER\n" +
                ")";
        try {
            mySqLiteDatabase.execSQL(query);
        } catch (Exception ex) {
            ex.printStackTrace();
            TastyToast.makeText(context, "Table Creation Failed", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
        }
    }

    public void saveData(long bus, long truck, long car, long bike, long cng, long cycle, long rickshaw, String date_recorded, String start_time, String end_time) {
        String query = "INSERT INTO " + tableName + " " +
                "(date_recorded, start_time, end_time, bus, truck, car, bike, cng, cycle, rickshaw)" + " " +
                "VALUES" + " " +
                "('" +
                date_recorded + "','" + start_time + "','" + end_time + "'," + bus + "," + truck + "," + car + "," + bike + "," + cng + "," + cycle + "," + rickshaw +
                ")";
        try {
            mySqLiteDatabase.execSQL(query);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public ArrayList<Record> retrieveData() {
        Cursor cursor = mySqLiteDatabase.rawQuery("SELECT * FROM count", null);

        int bus_index = cursor.getColumnIndex("bus");
        int truck_index = cursor.getColumnIndex("truck");
        int car_index = cursor.getColumnIndex("car");
        int bike_index = cursor.getColumnIndex("bike");
        int cng_index = cursor.getColumnIndex("cng");
        int cycle_index = cursor.getColumnIndex("cycle");
        int rickshaw_index = cursor.getColumnIndex("rickshaw");
        int dr_index = cursor.getColumnIndex("date_recorded");
        int st_index = cursor.getColumnIndex("start_time");
        int en_index = cursor.getColumnIndex("end_time");

        ArrayList<Record> recordList = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            do {
                Record record = new Record(cursor.getInt(bus_index),
                        cursor.getInt(truck_index),
                        cursor.getInt(car_index),
                        cursor.getInt(bike_index),
                        cursor.getInt(cng_index),
                        cursor.getInt(cycle_index),
                        cursor.getInt(rickshaw_index),
                        cursor.getString(dr_index),
                        cursor.getString(st_index),
                        cursor.getString(en_index));
                recordList.add(record);
            } while (cursor.moveToNext());
        }

        return recordList;
    }

    public void deleteData() {
        String query = "DELETE FROM " + tableName;
        mySqLiteDatabase.execSQL(query);
    }
}
