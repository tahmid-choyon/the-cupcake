package info.tahmidchoyon.vehiclecounter.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by tahmid on 05-Nov-17.
 */

public class Record implements Parcelable {

    public static final Creator<Record> CREATOR = new Creator<Record>() {
        @Override
        public Record createFromParcel(Parcel in) {
            return new Record(in);
        }

        @Override
        public Record[] newArray(int size) {
            return new Record[size];
        }
    };
    private int bus, truck, car, bike, cng, cycle, rickshaw;
    private String recordTime, startTime, endTime;

    public Record(int bus, int truck, int car, int bike, int cng, int cycle, int rickshaw, String recordTime, String startTime, String endTime) {
        this.bus = bus;
        this.truck = truck;
        this.car = car;
        this.bike = bike;
        this.cng = cng;
        this.cycle = cycle;
        this.rickshaw = rickshaw;
        this.recordTime = recordTime;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    protected Record(Parcel in) {
        bus = in.readInt();
        truck = in.readInt();
        car = in.readInt();
        bike = in.readInt();
        cng = in.readInt();
        cycle = in.readInt();
        rickshaw = in.readInt();
        recordTime = in.readString();
        startTime = in.readString();
        endTime = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(bus);
        parcel.writeInt(truck);
        parcel.writeInt(car);
        parcel.writeInt(bike);
        parcel.writeInt(cng);
        parcel.writeInt(cycle);
        parcel.writeInt(rickshaw);
        parcel.writeString(recordTime);
        parcel.writeString(startTime);
        parcel.writeString(endTime);
    }

    @Override
    public String toString() {
        return "======" + "\n\n" +
                "Bus= " + bus + "\n" +
                "Truck= " + truck + "\n" +
                "Car= " + car + "\n" +
                "Bike= " + bike + "\n" +
                "Cng= " + cng + "\n" +
                "Cycle= " + cycle + "\n" +
                "Rickshaw= " + rickshaw + "\n" +
                "RecordTime= " + recordTime + "\n" +
                "StartTime= " + startTime + "\n" +
                "EndTime= " + endTime + "\n\n" +
                "======\n\n";
    }
}
