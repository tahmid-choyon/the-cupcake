package com.tahmid.timerdemo;

import android.os.Handler;
import android.widget.TextView;

/**
 * Created by tahmid on 4/27/17.
 */

public class Execution implements Runnable {

    Handler handler;
    private int timer;
    TextView textView;

    public Execution(Handler handler, TextView textView) {
        this.handler = handler;
        this.textView = textView;
        timer = 0;
    }

    @Override
    public void run() {

        if (timer == 10) {
            textView.setText("10 seconds have passed....");
            return;
        }

        textView.setText("Seconds count: " + ++timer);

        handler.postDelayed(this, 1000);
    }
}
