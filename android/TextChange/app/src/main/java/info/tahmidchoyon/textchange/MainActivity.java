package info.tahmidchoyon.textchange;

import android.support.annotation.IntegerRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText editText_one, editText_two;
    private TextView resultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText_one = (EditText) findViewById(R.id.editText_one);
        editText_two = (EditText) findViewById(R.id.editText_two);
        resultTextView = (TextView) findViewById(R.id.result);
        resultTextView.setText("");

        editText_one.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (isNumber(s.toString()) && isNumber(editText_two.getText().toString())) {

                    int a = Integer.parseInt(editText_one.getText().toString());
                    int b = Integer.parseInt(editText_two.getText().toString());
                    int c = a + b;

                    resultTextView.setText(String.valueOf(c));
                } else if (s.length() == 0 && editText_two.getText().toString().isEmpty()) {
                    resultTextView.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editText_two.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (isNumber(editText_one.getText().toString()) && isNumber(s.toString())) {

                    int a = Integer.parseInt(editText_one.getText().toString());
                    int b = Integer.parseInt(editText_two.getText().toString());
                    int c = a + b;

                    resultTextView.setText(String.valueOf(c));
                } else if (s.length() == 0 && editText_one.getText().toString().isEmpty()) {
                    resultTextView.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private boolean isNumber(String string) {

        try {
            int num = Integer.parseInt(string);
        } catch (Exception ex) {
            return false;
        }

        return true;
    }
}
