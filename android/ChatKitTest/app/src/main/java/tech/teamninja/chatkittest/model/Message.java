package tech.teamninja.chatkittest.model;

import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.IUser;

import java.util.Date;

/**
 * Created by tahmid on 27-Sep-17.
 */

public class Message implements IMessage {

    private String id, text;
    private IUser user;
    private Date createdAt;

    public Message(String id, String text, IUser user, Date createdAt) {
        this.id = id;
        this.text = text;
        this.user = user;
        this.createdAt = createdAt;
    }

    /**
     * Returns message identifier
     *
     * @return the message id
     */
    @Override
    public String getId() {
        return this.id;
    }

    /**
     * Returns message text
     *
     * @return the message text
     */
    @Override
    public String getText() {
        return this.text;
    }

    /**
     * Returns message author. See the {@link IUser} for more details
     *
     * @return the message author
     */
    @Override
    public IUser getUser() {
        return this.user;
    }

    /**
     * Returns message creation date
     *
     * @return the message creation date
     */
    @Override
    public Date getCreatedAt() {
        return this.createdAt;
    }
}
