package tech.teamninja.chatkittest;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.stfalcon.chatkit.utils.DateFormatter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tech.teamninja.chatkittest.model.Leaderboard;
import tech.teamninja.chatkittest.model.Message;
import tech.teamninja.chatkittest.model.Question;
import tech.teamninja.chatkittest.model.User;

public class MainActivity extends AppCompatActivity implements DateFormatter.Formatter {

    public static final String[] ALL_QUESTIONS = {"Question: মঙ্গল গ্রহে কয়টি চাঁদ আছে?", "১টি", "Correct: ২টি", "৩টি", "৮টি ", "Question: মঙ্গল গ্রহে গড় তাপমাত্রা কত?", "Correct: -৬৬ ডিগ্রী সেলসিয়াস", "৫০ ডিগ্রী সেলসিয়াস", "০ ডিগ্রী সেলসিয়াস", "১০০০ ডিগ্রী সেলসিয়াস", "Question: মঙ্গল গ্রহ সূর্য থেকে কততম গ্রহ?", "প্রথম", "দ্বিতীয়", "তৃতীয়", "Correct: চতুর্থ", "Question: আমাদের পৃথিবীতে মঙ্গল গ্রহের মতো একটি জায়গা রয়েছে, জায়গাটির নাম কি এবং জায়গাটি কোথায় অবস্থিত?", "আটাকামা মরুভুমি, অস্ট্রেলিয়া", "Correct: চিলি আটাকামা মরুভুমি", "সাহারা মরুভুমি", "ইন্দাস ভ্যালি মরুভুমি", "Question: মঙ্গল গ্রহকে বলা হয় -", "Yellow Planet", "Blue Planet", "Correct: Red Planet", "Brown Planet", "Question: নিচের কোনটি মঙ্গল গ্রহের চাঁদ?", "মেটিস", "মুন", "ইউরোপ", "Correct: ফোবোস"};

    private MessageInput messageInput;
    private MessagesList messagesList;
    private Button btnA, btnB, btnC, btnD;
    private Toolbar toolbar;
    private TextView coinTextView;

    MessagesListAdapter<Message> messagesListAdapter;
    User me, bot;

    public List<Question> questions;

    private int index, score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        messageInput = (MessageInput) findViewById(R.id.input);
        messagesList = (MessagesList) findViewById(R.id.messagesList);

        btnA = (Button) findViewById(R.id.buttonA);
        btnB = (Button) findViewById(R.id.buttonB);
        btnC = (Button) findViewById(R.id.buttonC);
        btnD = (Button) findViewById(R.id.buttonD);
        coinTextView = (TextView) findViewById(R.id.coin_collection);

        index = 0;
        score = 0;

        toolbar = (Toolbar) findViewById(R.id.customToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        questions = new ArrayList<>();
        addQuestions(questions);

        me = new User("tahmid", "Tahmid Choyon", null);
        bot = new User("pobot", "Sohana", null);

        MessagesListAdapter.HoldersConfig holdersConfig = new MessagesListAdapter.HoldersConfig();
        holdersConfig.setIncomingLayout(R.layout.incoming_message_text_layout);
        holdersConfig.setOutcomingLayout(R.layout.outcoming_message_text_layout);

        messagesListAdapter = new MessagesListAdapter<>(me.getId(), holdersConfig, null);
        messagesList.setAdapter(messagesListAdapter);

        addBotResponse(bot, "হ্যালো", messagesListAdapter, 2000L);
        addBotResponse(bot, "আশা করি আপনি মঙ্গল গ্রহ সম্পর্কে অনেক কিছু জানতে পেরেছেন।", messagesListAdapter, 5000L);
        addBotResponse(bot, "এইবার তাহলে প্রশ্নোত্তর পর্বে আসা যাক", messagesListAdapter, 8000L);
        addQuestionFromBot(bot, messagesListAdapter);

        /*messageInput.setInputListener(new MessageInput.InputListener() {
            *//**
         * Fires when user presses 'send' button.
         *
         * @param input input entered by user
         * @return if input text is valid, you must return {@code true} and input will be cleared, otherwise return false.
         *//*
            @Override
            public boolean onSubmit(CharSequence input) {

                Message message = new Message("1", input.toString(), me, new Date());
                messagesListAdapter.addToStart(message, true);
                BotResponse botResponse = new BotResponse(bot, messagesListAdapter, input.toString());
                botResponse.sendBotResponse();
                return true;
            }
        });*/

        btnA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                messagesListAdapter.addToStart(new Message("1", btnA.getText().toString(), me, new Date()), true);
                botResponseOnAnswer(questions.get(index - 1), btnA.getText().toString(), bot, messagesListAdapter);
            }
        });

        btnB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                messagesListAdapter.addToStart(new Message("1", btnB.getText().toString(), me, new Date()), true);
                botResponseOnAnswer(questions.get(index - 1), btnB.getText().toString(), bot, messagesListAdapter);
            }
        });

        btnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                messagesListAdapter.addToStart(new Message("1", btnC.getText().toString(), me, new Date()), true);
                try {
                    botResponseOnAnswer(questions.get(index - 1), btnC.getText().toString(), bot, messagesListAdapter);
                } catch (Exception ex) {

                }
            }
        });

        btnD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                messagesListAdapter.addToStart(new Message("1", btnD.getText().toString(), me, new Date()), true);
                botResponseOnAnswer(questions.get(index - 1), btnD.getText().toString(), bot, messagesListAdapter);
            }
        });

    }

    @Override
    public String format(Date date) {
        if (DateFormatter.isToday(date)) {
            return DateFormatter.format(date, DateFormatter.Template.TIME);
        } else if (DateFormatter.isYesterday(date)) {
            return getString(R.string.date_header_yesterday);
        } else {
            return DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH_YEAR);
        }
    }

    public void addQuestions(List<Question> questions) {
        for (int i = 0; i < ALL_QUESTIONS.length; i += 5) {

            Question ques = new Question(ALL_QUESTIONS[i],
                    ALL_QUESTIONS[i + 1],
                    ALL_QUESTIONS[i + 2],
                    ALL_QUESTIONS[i + 3],
                    ALL_QUESTIONS[i + 4]);

            questions.add(ques);
        }
    }

    public void addBotResponse(User bot, String text, MessagesListAdapter<Message> messageMessagesListAdapter, long delay) {
        disableButtons();
        Message msg = new Message("2", text, bot, new Date());
        TemporaryExecution temporaryExecution = new TemporaryExecution(delay, 20, text, messageMessagesListAdapter, msg);
        temporaryExecution.start();
    }

    public void disableButtons() {
        btnA.setActivated(false);
        btnB.setActivated(false);
        btnC.setActivated(false);
        btnD.setActivated(false);
    }

    public void enableButtons() {
        btnA.setActivated(true);
        btnB.setActivated(true);
        btnC.setActivated(true);
        btnD.setActivated(true);
    }

    public boolean evaluateResponse(Question question, String answer) {
        if (question.getCorrectOption().equals(answer)) {
            return true;
        }
        return false;
    }

    public void botResponseOnAnswer(Question question, String answer, User bot, MessagesListAdapter<Message> messageMessagesListAdapter) {
        if (!evaluateResponse(question, answer)) {
            addBotResponse(bot, "ভুল উত্তর! পরের গুলো আশা করি সঠিক হবে", messageMessagesListAdapter, 3000L);
        } else {
            addBotResponse(bot, "আপনি চমৎকার খেলছেন! আপনার উত্তরটি সঠিক হয়েছে!", messageMessagesListAdapter, 3000L);
            coinTextView.setText(String.valueOf(++score));
        }
        addQuestionFromBot(bot, messageMessagesListAdapter);
    }

    public void addQuestionFromBot(User bot, MessagesListAdapter<Message> messageMessagesListAdapter) {
        enableButtons();
        if (index == 0) {
            addBotResponse(bot, "প্রথম প্রশ্ন", messageMessagesListAdapter, 11000L);
            addBotResponse(bot, questions.get(0).toString(), messageMessagesListAdapter, 14000L);
            index++;
        } else if (index > 5) {
            addBotResponse(bot, "আপনি ভালো খেলেছেন। আপনার স্কোর " + score, messageMessagesListAdapter, 4000L);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(getApplicationContext(), Leaderboard.class);
                    startActivity(intent);
                }
            }, 6000);
        } else {
            addBotResponse(bot, "পরের প্রশ্ন", messageMessagesListAdapter, 4000L);
            addBotResponse(bot, questions.get(index).toString(), messageMessagesListAdapter, 6000L);
            index++;
        }
    }
}