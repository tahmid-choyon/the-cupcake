package tech.teamninja.chatkittest;

import com.stfalcon.chatkit.messages.MessagesListAdapter;

import java.util.Date;
import java.util.Random;

import tech.teamninja.chatkittest.model.Message;
import tech.teamninja.chatkittest.model.User;

/**
 * Created by tahmid on 27-Sep-17.
 */

public class BotResponse {

    private static String[] RESPONSE_LIST = {"Hi", "I am Napa Apa, the PO Bot", "Lorem Ipsum!", "Lalala", ":D", ":P", "Napa khao!"};
    private User bot;
    private MessagesListAdapter<Message> messageMessagesListAdapterBot;
    private String lastUserMessage;

    public BotResponse(User bot, MessagesListAdapter<Message> messageMessagesListAdapterBot, String lastUserMessage) {
        this.bot = bot;
        this.messageMessagesListAdapterBot = messageMessagesListAdapterBot;
        this.lastUserMessage = lastUserMessage;
    }

    public void sendBotResponse() {
        Message message = new Message(bot.getId(), getRandomResponse(), bot, new Date());
        Execution execution = new Execution(1500, 750, messageMessagesListAdapterBot, message);
        execution.start();
    }

    private String getRandomResponse() {
        if (matchUserMessage(new String[]{"hi", "hello", "hey", "howdy"})) {
            return "আপনি চমৎকার খেলছেন! আপনার উত্তরটি সঠিক হয়েছে!\n";
        } else if (matchUserMessage(new String[]{"how are you?", "how are you", "how're you"})) {
            return "Bhaalo. Tumi?";
        } else if (matchUserMessage(new String[]{"any latest news?", "any news", "any news?"})) {
            return "Check this out\nhttp://www.thedailystar.net/";
        } else if (matchUserMessage(new String[]{"tell me a joke"})) {
            return "you are a joke \uD83D\uDE0A";
        } else if (matchUserMessage(new String[]{"okay", "ok"})) {
            return "\uD83D\uDC4D";
        } else {
            return "okay";
        }
    }

    private boolean matchUserMessage(String[] texts) {
        for (String text : texts) {
            if (lastUserMessage.toLowerCase().equals(text))
                return true;
        }
        return false;
    }
}
