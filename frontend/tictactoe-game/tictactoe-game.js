// grab all squares
var squares = document.querySelectorAll("td");


// restart btn
var restartButton = document.querySelector("#restart-btn");
restartButton.addEventListener("click", restartGame);


// restart game
function restartGame() {
    for (let index = 0; index < squares.length; index++) {
        squares[index].textContent = "";

    }
}

// change marker
function changeMarkerToX() {
    this.textContent = "X";
}

function changeMarkerToO() {
    this.textContent = "O";
}

// add event listener to all squares
for (let index = 0; index < squares.length; index++) {
    squares[index].addEventListener("click", changeMarkerToX);
    squares[index].addEventListener("dblclick", changeMarkerToO);
}