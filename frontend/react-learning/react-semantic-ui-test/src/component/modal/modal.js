import React from 'react'
import {Card, Grid, GridColumn, Header, Icon, Image, Modal} from 'semantic-ui-react'
import './modal.css'

class CustomModal extends React.Component {
    render() {
        return (
            <div className="container">
                <Grid container columns={this.props.columns}>

                    {this.props.cardItems.map(item => {
                        return (
                            <GridColumn>
                                <Modal size="fullscreen" dimmer="inverted" trigger={
                                    <Card
                                        image={item.avatar}
                                        header={item.name}
                                        meta={item.date}
                                        description={item.description}
                                        extra={
                                            <a href={"#"}>
                                                <Icon name='imdb'/>
                                                {item.rating}
                                            </a>
                                        }
                                    />
                                }>
                                    <Modal.Header>{item.name}</Modal.Header>
                                    <Modal.Content image>
                                        <Image wrapped size='medium' src={item.avatar}/>
                                        <Modal.Description>
                                            <Header>Hi, I am {item.name}</Header>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque
                                                dignissimos distinctio eaque molestiae officia optio perspiciatis
                                                reiciendis tempore vero voluptas? Error laboriosam mollitia nesciunt
                                                nobis nulla quasi sit veniam vero.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum cumque eos,
                                                facilis incidunt iure magni necessitatibus voluptate! Commodi eligendi
                                                maiores mollitia natus. Corporis delectus iste magni officiis provident
                                                quae recusandae.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur
                                                delectus dolores enim incidunt natus nihil, quis quod. Assumenda dicta
                                                eaque id natus nemo, pariatur quibusdam ratione suscipit tenetur veniam.
                                                Quibusdam.</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus
                                                doloribus eveniet expedita maiores molestiae praesentium quibusdam quos
                                                ratione ullam. Accusantium consequuntur delectus ipsum iure nam neque
                                                quam, reiciendis rerum tempore?</p>
                                        </Modal.Description>
                                    </Modal.Content>
                                </Modal>
                            </GridColumn>
                        )
                    })}

                </Grid>
            </div>
        )
    }
}

export default CustomModal;