import React from 'react';
import CustomMenu from "./component/menu/menu";
import CustomModal from "./component/modal/modal";


const menuItemProps = {
    menuItems: [
        {name: 'home'},
        {name: 'browse'}
    ]
};
const cardItemProps = {
    columns: 3,
    cardItems: [
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/matthew.png",
            name: "Matthew",
            date: "2018",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/matthew",
            rating: "4.3"
        },
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/elliot.jpg",
            name: "Elliot",
            date: "2005",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/elliot",
            rating: "4.1"
        },
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/daniel.jpg",
            name: "Daniel",
            date: "1996",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/daniel",
            rating: "3.8"
        },
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/matthew.png",
            name: "Matthew",
            date: "2018",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/matthew",
            rating: "4.3"
        },
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/elliot.jpg",
            name: "Elliot",
            date: "2005",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/elliot",
            rating: "4.1"
        },
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/daniel.jpg",
            name: "Daniel",
            date: "1996",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/daniel",
            rating: "3.8"
        },
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/matthew.png",
            name: "Matthew",
            date: "2018",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/matthew",
            rating: "4.3"
        },
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/elliot.jpg",
            name: "Elliot",
            date: "2005",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/elliot",
            rating: "4.1"
        },
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/daniel.jpg",
            name: "Daniel",
            date: "1996",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/daniel",
            rating: "3.8"
        },
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/matthew.png",
            name: "Matthew",
            date: "2018",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/matthew",
            rating: "4.3"
        },
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/elliot.jpg",
            name: "Elliot",
            date: "2005",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/elliot",
            rating: "4.1"
        },
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/daniel.jpg",
            name: "Daniel",
            date: "1996",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/daniel",
            rating: "3.8"
        },
        {
            avatar: "https://react.semantic-ui.com/images/avatar/large/elliot.jpg",
            name: "Elliot",
            date: "2005",
            description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad architecto deserunt dolore doloremque eius error expedita, facilis fugiat id illum labore libero maiores nisi nobis optio perspiciatis sit veniam!",
            href: "/elliot",
            rating: "4.1"
        }
    ]
};

class App extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container">
                <CustomMenu {...menuItemProps} />
                <div className="ui divider"></div>
                <CustomModal {...cardItemProps} />
            </div>
        )
    }
}

export default App;
