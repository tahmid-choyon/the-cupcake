import React from 'react'
import OutputScreenRow from './outputScreenRow'

const OutputScreen = (props) => {
    return (
        <div className="screen">
            <OutputScreenRow value={props.expression}/>
            <OutputScreenRow value={props.answer}/>
        </div>
    )
};

export default OutputScreen;